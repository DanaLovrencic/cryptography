#ifndef IDENTIFIER_HPP
#define IDENTIFIER_HPP

namespace ID {
enum : int { symmetric_rb, envelope_btn, signature_btn, seal_btn };
}

#endif
