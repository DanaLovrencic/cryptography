#include "MainFrame.hpp"

#include <iostream>

MainFrame::MainFrame()
    : wxFrame(nullptr, wxID_ANY, "DigitalESS", wxDefaultPosition, {239, 325}),
      algo_panel(new AlgorithmsPanel(this)),
      files_panel(new FilesPanel(this)),
      status_panel(new StatusPanel(this)),
      controls_panel(
          new ControlsPanel(this, *algo_panel, *files_panel, *status_panel))
{
    auto sizer = new wxBoxSizer(wxVERTICAL);
    sizer->AddSpacer(7);
    sizer->Add(algo_panel, 1, wxEXPAND | wxLEFT | wxRIGHT, 7);
    sizer->AddSpacer(10);
    sizer->Add(files_panel, 1, wxEXPAND | wxLEFT | wxRIGHT, 7);
    sizer->AddSpacer(10);
    sizer->Add(controls_panel, 1, wxEXPAND | wxLEFT | wxRIGHT, 7);
    sizer->AddSpacer(10);
    sizer->Add(status_panel, 2, wxEXPAND | wxLEFT | wxRIGHT, 7);
    sizer->AddSpacer(7);
    SetSizer(sizer);
}

StatusPanel& MainFrame::get_status_panel()
{
    return *status_panel;
}
