#include "FilesPanel.hpp"

#include <vector>

// Za lakše mijenjanje datoteka!
namespace Examples {
std::vector<std::string> in_files = {"envelope_create.json",
                                     "envelope_read.json",
                                     "signature_create.json",
                                     "signature_verify.json",
                                     "seal_create.json",
                                     "seal_read.json"};

std::vector<std::string> out_files = {"envelope_create_out.json",
                                      "envelope_read_out.json",
                                      "signature_create_out.json",
                                      "signature_verify_out.json",
                                      "seal_create_out.json",
                                      "seal_read_out.json"};
}

FilesPanel::FilesPanel(wxWindow* parent)
    : wxPanel(parent),
      input_file_box(new wxTextCtrl(this, wxID_ANY, Examples::in_files[3])),
      output_file_box(new wxTextCtrl(this, wxID_ANY, Examples::out_files[3]))
{
    auto sizer = new wxBoxSizer(wxVERTICAL);
    sizer->AddStretchSpacer();
    sizer->Add(input_file_box, 0, wxEXPAND);
    sizer->AddSpacer(3);
    sizer->Add(output_file_box, 0, wxEXPAND);
    sizer->AddStretchSpacer();
    SetSizer(sizer);
}
