#ifndef DIGITAL_ENVELOPE_HPP
#define DIGITAL_ENVELOPE_HPP

#include "Algorithm.hpp"

#include <botan/secmem.h>
#include <botan/rsa.h>
#include <botan/pubkey.h>

class DigitalEnvelope {
  private:
    Botan::secure_vector<uint8_t> encrypted_key;
    Botan::secure_vector<uint8_t> cyphertext;

  public:
    // Creates digital envelope based on chosen algorithms.
    static DigitalEnvelope create(const std::string& key,
                                  const std::string& plaintext,
                                  const std::vector<uint8_t> iv,
                                  Botan::PK_Encryptor_EME& rsa_encryptor,
                                  SymmetricAlgorithm sa,
                                  SymmetricMode mode);
    static DigitalEnvelope create(const std::string& encrypted_key,
                                  const std::string& cyphertext);
    Botan::secure_vector<uint8_t> get_encrypted_key() const;
    Botan::secure_vector<uint8_t> get_cyphertext() const;
    Botan::secure_vector<uint8_t>
    get_decrypted_key(Botan::PK_Decryptor_EME& rsa_decryptor) const;
    Botan::secure_vector<uint8_t>
    get_decrypted_text(const std::string& key,
                       const std::vector<uint8_t>& iv,
                       SymmetricAlgorithm sa,
                       SymmetricMode mode) const;

  private:
    DigitalEnvelope(Botan::secure_vector<uint8_t>&& encrypted_key,
                    Botan::secure_vector<uint8_t>&& cyphertext);
};

#endif
