#ifndef STATUS_PANEL_HPP
#define STATUS_PANEL_HPP

#include <wx/wx.h>

class StatusPanel : public wxPanel {
  private:
    wxStaticText* label;

  public:
    StatusPanel(wxWindow* parent);

    void set_status(const std::string& status, bool success = true);
};

#endif
