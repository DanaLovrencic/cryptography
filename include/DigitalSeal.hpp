#ifndef DIGITAL_SEAL_HPP
#define DIGITAL_SEAL_HPP

#include "DigitalSignature.hpp"
#include "DigitalEnvelope.hpp"

class DigitalSeal {
  private:
    DigitalSignature signature;
    DigitalEnvelope envelope;

  public:
    DigitalSeal(DigitalSignature, DigitalEnvelope);
};

#endif
