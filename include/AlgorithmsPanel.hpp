#ifndef ALGORITHMS_PANEL
#define ALGORITHMS_PANEL

#include <wx/wx.h>

#include <array>

class AlgorithmsPanel : public wxPanel {
  public:
    wxRadioBox* symmetric_rb  = nullptr;
    wxRadioBox* asymmetric_rb = nullptr;
    wxRadioBox* hash_rb       = nullptr;

    wxChoice* symmetric_c  = nullptr;
    wxChoice* asymmetric_c = nullptr;
    wxChoice* hash_c       = nullptr;

    wxChoice* block_mode_c = nullptr;

  public:
    AlgorithmsPanel(wxWindow* parent);

  private:
    void on_symmetric_rb_click(wxCommandEvent& event);

    wxDECLARE_EVENT_TABLE();
};

#endif
