#include "FileInput.hpp"

#include <botan/hex.h>

#include <iostream>
#include <fstream>
#include <sstream>

using namespace nlohmann;

namespace Envelope {
CreateObject::CreateObject(json json_obj)
{
    create        = json_obj["envelope"]["create"];
    iv            = Botan::hex_decode(json_obj["envelope"]["iv"]);
    plaintext     = json_obj["envelope"]["plaintext"];
    symmetric_key = json_obj["envelope"]["symmetric_key"];
    // Za javni kljuc primatelja.
    e = json_obj["envelope"]["e"];
    n = Botan::BigInt(Botan::hex_decode(json_obj["envelope"]["n"]));
}

void CreateObject::write_to_file(const std::string& encrypted_key,
                                 const std::string& cyphertext,
                                 const std::string& file_name,
                                 bool append)
{
    json json_out;
    std::ofstream out(file_name,
                      append ? std::ios_base::app : std::ios_base::out);

    json_out["envelope"]["encrypted_key"] = encrypted_key;
    json_out["envelope"]["cyphertext"]    = cyphertext;

    out << json_out;
}

void CreateObject::check_input(SymmetricAlgorithm sa,
                               AsymmetricAlgorithm aa) const
{
    unsigned long sa_key_len;
    unsigned long aa_key_len;

    switch (sa) {
        case SymmetricAlgorithm::AES128: sa_key_len = 128; break;
        case SymmetricAlgorithm::AES192: sa_key_len = 192; break;
        case SymmetricAlgorithm::AES256: sa_key_len = 256; break;
        case SymmetricAlgorithm::TripleDES: sa_key_len = 192; break;
    }

    switch (aa) {
        case AsymmetricAlgorithm::RSA1024: aa_key_len = 1024; break;
        case AsymmetricAlgorithm::RSA2048: aa_key_len = 2048; break;
        case AsymmetricAlgorithm::RSA3072: aa_key_len = 3072; break;
    }

    if (symmetric_key.length() * 4 != sa_key_len) {
        std::stringstream ss;
        ss << "Incorrect symmetric key length! Expected " << sa_key_len
           << " bits but " << symmetric_key.length() * 4 << " were provided.";
        throw std::runtime_error(ss.str());
    }

    if (n.bits() != aa_key_len) {
        std::stringstream ss;
        ss << "Incorrect asymmetric key length! Expected " << aa_key_len
           << " bits but " << n.bits() << " were provided.";
        throw std::runtime_error(ss.str());
    }

    if (sa == SymmetricAlgorithm::TripleDES && iv.size() != 8) {
        std::stringstream ss;
        ss << "Incorrect initialization vector size! Expected " << 8 * 8
           << " bits but " << iv.size() * 8 << " were provided.";
        throw std::runtime_error(ss.str());
    } else if (sa != SymmetricAlgorithm::TripleDES && iv.size() != 16) {
        std::stringstream ss;
        ss << "Incorrect initialization vector size! Expected " << 16 * 8
           << " bits but " << iv.size() * 8 << " were provided.";
        throw std::runtime_error(ss.str());
    }
}

void ReadObject::write_decr_to_file(const std::string& decrypted_key,
                                    const std::string& decrypted_text,
                                    const std::string& file_name,
                                    bool append)
{
    json json_out;
    std::ofstream out(file_name,
                      append ? std::ios_base::app : std::ios_base::out);

    json_out["envelope"]["decrypted_key"]  = decrypted_key;
    json_out["envelope"]["decrypted_text"] = decrypted_text;

    out << json_out;
}

void ReadObject::check_input(SymmetricAlgorithm sa,
                             AsymmetricAlgorithm aa) const
{
    unsigned long aa_key_len;

    switch (aa) {
        case AsymmetricAlgorithm::RSA1024: aa_key_len = 1024; break;
        case AsymmetricAlgorithm::RSA2048: aa_key_len = 2048; break;
        case AsymmetricAlgorithm::RSA3072: aa_key_len = 3072; break;
    }

    if (n.bits() != aa_key_len) {
        std::stringstream ss;
        ss << "Incorrect asymmetric key length! Expected " << aa_key_len
           << " bits but " << n.bits() << " were provided.";
        throw std::runtime_error(ss.str());
    }

    if (p * q != n) {
        std::stringstream ss;
        ss << "Key inconsistency (p * q != n)";
        throw std::runtime_error(ss.str());
    }

    if (sa == SymmetricAlgorithm::TripleDES && iv.size() != 8) {
        std::stringstream ss;
        ss << "Incorrect initialization vector size! Expected " << 8 * 8
           << " bits but " << iv.size() * 8 << " were provided.";
        throw std::runtime_error(ss.str());
    } else if (sa != SymmetricAlgorithm::TripleDES && iv.size() != 16) {
        std::stringstream ss;
        ss << "Incorrect initialization vector size! Expected " << 16 * 8
           << " bits but " << iv.size() * 8 << " were provided.";
        throw std::runtime_error(ss.str());
    }
}

ReadObject::ReadObject(json json_obj)
{
    create        = json_obj["envelope"]["create"];
    iv            = Botan::hex_decode(json_obj["envelope"]["iv"]);
    cyphertext    = json_obj["envelope"]["cyphertext"];
    encrypted_key = json_obj["envelope"]["encrypted_key"];
    // Za privatni kljuc primatelja.
    e = json_obj["envelope"]["e"];
    d = Botan::BigInt(Botan::hex_decode(json_obj["envelope"]["d"]));
    p = Botan::BigInt(Botan::hex_decode(json_obj["envelope"]["p"]));
    q = Botan::BigInt(Botan::hex_decode(json_obj["envelope"]["q"]));
    n = Botan::BigInt(Botan::hex_decode(json_obj["envelope"]["n"]));
}
}

namespace Signature {
CreateObject::CreateObject(json json_obj)
{
    verify    = json_obj["signature"]["verify"];
    plaintext = json_obj["signature"]["plaintext"];
    // Za privatni kljuc posiljatelja.
    e = json_obj["signature"]["e"];
    d = Botan::BigInt(Botan::hex_decode(json_obj["signature"]["d"]));
    n = Botan::BigInt(Botan::hex_decode(json_obj["signature"]["n"]));
    p = Botan::BigInt(Botan::hex_decode(json_obj["signature"]["p"]));
    q = Botan::BigInt(Botan::hex_decode(json_obj["signature"]["q"]));
}

void CreateObject::write_to_file(const std::string& signature,
                                 const std::string& file_name,
                                 bool append)
{
    json json_out;
    std::ofstream out(file_name,
                      append ? std::ios_base::app : std::ios_base::out);

    json_out["signature"]["signature"] = signature;

    out << json_out;
}

void CreateObject::check_input(AsymmetricAlgorithm aa) const
{
    unsigned long aa_key_len;

    switch (aa) {
        case AsymmetricAlgorithm::RSA1024: aa_key_len = 1024; break;
        case AsymmetricAlgorithm::RSA2048: aa_key_len = 2048; break;
        case AsymmetricAlgorithm::RSA3072: aa_key_len = 3072; break;
    }

    if (n.bits() != aa_key_len) {
        std::stringstream ss;
        ss << "Incorrect asymmetric key length! Expected " << aa_key_len
           << " bits but " << n.bits() << " were provided.";
        throw std::runtime_error(ss.str());
    }

    if (p * q != n) {
        std::stringstream ss;
        ss << "Key inconsistency (p * q != n)";
        throw std::runtime_error(ss.str());
    }
}

VerifyObject::VerifyObject(json json_obj)
{
    verify    = json_obj["signature"]["verify"];
    plaintext = json_obj["signature"]["plaintext"];
    signature = json_obj["signature"]["signature"];
    // Za javni kljuc posiljatelja.
    e = json_obj["signature"]["e"];
    n = Botan::BigInt(Botan::hex_decode(json_obj["signature"]["n"]));
}

void VerifyObject::write_to_file(bool correct,
                                 const std::string& file_name,
                                 bool append)
{
    json json_out;
    std::ofstream out(file_name,
                      append ? std::ios_base::app : std::ios_base::out);

    json_out["signature"]["correct"] = correct;

    out << json_out;
}

void VerifyObject::check_input(AsymmetricAlgorithm aa) const
{
    unsigned long aa_key_len;

    switch (aa) {
        case AsymmetricAlgorithm::RSA1024: aa_key_len = 1024; break;
        case AsymmetricAlgorithm::RSA2048: aa_key_len = 2048; break;
        case AsymmetricAlgorithm::RSA3072: aa_key_len = 3072; break;
    }

    if (n.bits() != aa_key_len) {
        std::stringstream ss;
        ss << "Incorrect asymmetric key length! Expected " << aa_key_len
           << " bits but " << n.bits() << " were provided.";
        throw std::runtime_error(ss.str());
    }
}
}
