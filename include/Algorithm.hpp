#ifndef ALGORITHM_HPP
#define ALGORITHM_HPP

#include <botan/cipher_mode.h>

enum class SymmetricAlgorithm { AES128, AES192, AES256, TripleDES };

enum class SymmetricMode { CBC, CFB };

enum class AsymmetricAlgorithm { RSA1024, RSA2048, RSA3072 };

enum class HashType { SHA256, SHA384, SHA512 };

std::unique_ptr<Botan::Cipher_Mode>
create(SymmetricAlgorithm algorithm, SymmetricMode mode, bool encrypt);

int get_modulus_size(AsymmetricAlgorithm algorithm);
int get_iv_size(SymmetricAlgorithm algorithm);
std::string generate_key(int key_length);
std::string get_hash_type(HashType hash_type);

SymmetricAlgorithm get_symmetric_algorithm(int index_algo, int index_key_len);
AsymmetricAlgorithm get_asymmetric_algorithm(int index_algo, int index_key_len);
SymmetricMode get_symmetric_mode(int index_mode);
HashType get_hash(int index_algo, int index_hash);

#endif
