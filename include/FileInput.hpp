#ifndef FILE_INPUT_HPP
#define FILE_INPUT_HPP

#include "Algorithm.hpp"

#include "json.hpp"
#include "botan/bigint.h"

namespace Envelope {
struct ReadObject {
    bool create;
    std::string encrypted_key;
    std::string cyphertext;
    std::vector<uint8_t> iv;
    Botan::BigInt p;
    Botan::BigInt d;
    Botan::BigInt q;
    Botan::BigInt n;
    int e;

    ReadObject(nlohmann::json json_obj);
    void check_input(SymmetricAlgorithm sa, AsymmetricAlgorithm aa) const;
    static void write_decr_to_file(const std::string& decrypted_key,
                                   const std::string& decrypted_text,
                                   const std::string& file_name,
                                   bool append);
};

struct CreateObject {
    bool create;
    std::string symmetric_key;
    std::string plaintext;
    std::vector<uint8_t> iv;
    Botan::BigInt n;
    int e;

    CreateObject(nlohmann::json json_obj);
    void check_input(SymmetricAlgorithm sa, AsymmetricAlgorithm aa) const;
    static void write_to_file(const std::string& encrypted_key,
                              const std::string& cyphertext,
                              const std::string& file_name,
                              bool append);
};
}

namespace Signature {
struct CreateObject {
    bool verify;
    std::string plaintext;
    int e;
    Botan::BigInt p;
    Botan::BigInt d;
    Botan::BigInt q;
    Botan::BigInt n;

    CreateObject(nlohmann::json json_obj);
    void check_input(AsymmetricAlgorithm aa) const;
    static void write_to_file(const std::string& signature,
                              const std::string& file_name,
                              bool append);
};

struct VerifyObject {
    bool verify;
    std::string plaintext;
    std::string signature;
    int e;
    Botan::BigInt n;

    VerifyObject(nlohmann::json json_obj);
    void check_input(AsymmetricAlgorithm aa) const;
    static void
    write_to_file(bool correct, const std::string& file_name, bool append);
};
}

#endif
