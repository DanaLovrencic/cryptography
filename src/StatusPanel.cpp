#include "StatusPanel.hpp"

StatusPanel::StatusPanel(wxWindow* parent)
    : wxPanel(parent),
      label(new wxStaticText(this,
                             wxID_ANY,
                             "Ready!",
                             wxDefaultPosition,
                             wxDefaultSize,
                             wxALIGN_CENTER))
{
    auto sizer = new wxBoxSizer(wxVERTICAL);
    sizer->Add(label, 0, wxEXPAND);
    SetSizer(sizer);
}

void StatusPanel::set_status(const std::string& status, bool success)
{
    auto color = success ? wxColor(10, 200, 10) : wxColor(240, 10, 10);
    label->SetForegroundColour(color);
    label->SetLabel(status);
    label->Wrap(220);
    label->Center();
}
