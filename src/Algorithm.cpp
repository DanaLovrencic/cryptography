#include "Algorithm.hpp"

std::unique_ptr<Botan::Cipher_Mode>
create(SymmetricAlgorithm algorithm, SymmetricMode mode, bool encrypt)
{
    std::string algo_name;
    switch (algorithm) {
        case SymmetricAlgorithm::AES128: algo_name = "AES-128"; break;
        case SymmetricAlgorithm::AES192: algo_name = "AES-192"; break;
        case SymmetricAlgorithm::AES256: algo_name = "AES-256"; break;
        case SymmetricAlgorithm::TripleDES: algo_name = "TripleDES"; break;
        default: throw std::logic_error("Unhandled symmetric algorithm enum.");
    }

    std::string mode_name;
    switch (mode) {
        case SymmetricMode::CBC: mode_name += "CBC/PKCS7"; break;
        case SymmetricMode::CFB: mode_name += "CFB"; break;
        default: throw std::logic_error("Unhandled symmetric mode enum.");
    }

    std::string algo_spec = algo_name + '/' + mode_name;
    if (encrypt) {
        return Botan::Cipher_Mode::create(algo_spec, Botan::ENCRYPTION);
    } else {
        return Botan::Cipher_Mode::create(algo_spec, Botan::DECRYPTION);
    }
}

int get_modulus_size(AsymmetricAlgorithm algorithm)
{
    int modulus_size;
    switch (algorithm) {
        case AsymmetricAlgorithm::RSA1024: modulus_size = 1024; break;
        case AsymmetricAlgorithm::RSA2048: modulus_size = 2048; break;
        case AsymmetricAlgorithm::RSA3072: modulus_size = 3072; break;
        default: throw std::logic_error("Unhandled asymmetric algorithm enum.");
    }
    return modulus_size;
}

int get_iv_size(SymmetricAlgorithm algorithm)
{
    int iv_size;
    if (algorithm == SymmetricAlgorithm::TripleDES) {
        iv_size = 8;
    } else {
        iv_size = 16;
    }
    return iv_size;
}

std::string get_hash_type(HashType hash_type)
{
    std::string type;

    switch (hash_type) {
        case HashType::SHA256: type = "256"; break;
        case HashType::SHA384: type = "384"; break;
        case HashType::SHA512: type = "512"; break;
        default: throw std::logic_error("Unhandled hash type enum.");
    }

    return "EMSA1(SHA-" + type + ")";
}

SymmetricMode get_symmetric_mode(int index_mode)
{
    SymmetricMode mode;
    switch (index_mode) {
        case 0: mode = SymmetricMode::CBC; break;
        case 1: mode = SymmetricMode::CFB; break;
        default: throw std::logic_error("Unhandled symmetric mode enum.");
    }
    return mode;
}

SymmetricAlgorithm get_symmetric_algorithm(int index_algo, int index_key_len)
{
    SymmetricAlgorithm sa;
    switch (index_algo) {
        case 0:
            switch (index_key_len) {
                case 0: sa = SymmetricAlgorithm::AES128; break;
                case 1: sa = SymmetricAlgorithm::AES192; break;
                case 2: sa = SymmetricAlgorithm::AES256; break;
            }
            break;
        case 1:
            switch (index_key_len) {
                case 0: sa = SymmetricAlgorithm::TripleDES; break;
            }
            break;
        default: throw std::logic_error("Unhandled symmetric algorithm enum.");
    }
    return sa;
}

AsymmetricAlgorithm get_asymmetric_algorithm(int index_algo, int index_key_len)
{
    AsymmetricAlgorithm aa;
    switch (index_algo) {
        case 0:
            switch (index_key_len) {
                case 0: aa = AsymmetricAlgorithm::RSA1024; break;
                case 1: aa = AsymmetricAlgorithm::RSA2048; break;
                case 2: aa = AsymmetricAlgorithm::RSA3072; break;
            }
            break;
        default: throw std::logic_error("Unhandled asymmetric algorithm enum.");
    }
    return aa;
}

HashType get_hash(int index_algo, int index_hash)
{
    HashType type;
    switch (index_algo) {
        case 0:
            switch (index_hash) {
                case 0: type = HashType::SHA256; break;
                case 1: type = HashType::SHA384; break;
                case 2: type = HashType::SHA512; break;
                default:
                    throw std::logic_error("Unhandled hash type enum.");
                    break;
            }
            break;
        default: throw std::logic_error("Unhandled hash type enum."); break;
    }
    return type;
}
