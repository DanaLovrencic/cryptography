#include "ControlsPanel.hpp"
#include "Identifier.hpp"

#include "DigitalEnvelope.hpp"
#include "DigitalSignature.hpp"
#include "Algorithm.hpp"

#include <botan/hex.h>
#include <botan/auto_rng.h>
#include <botan/rsa.h>

#include <fstream>

using namespace nlohmann;

ControlsPanel::ControlsPanel(wxWindow* parent,
                             const AlgorithmsPanel& algo_panel,
                             const FilesPanel& files_panel,
                             StatusPanel& status_panel)
    : wxPanel(parent),
      algo_panel(algo_panel),
      files_panel(files_panel),
      status_panel(status_panel),
      envelope_btn(new wxButton(this, ID::envelope_btn, "Envelope")),
      signature_btn(new wxButton(this, ID::signature_btn, "Signature")),
      seal_btn(new wxButton(this, ID::seal_btn, "Seal"))
{
    auto sizer = new wxBoxSizer(wxHORIZONTAL);
    sizer->Add(envelope_btn);
    sizer->AddSpacer(3);
    sizer->Add(signature_btn);
    sizer->AddSpacer(3);
    sizer->Add(seal_btn);
    SetSizer(sizer);
}

void ControlsPanel::on_envelope_btn_pressed(wxCommandEvent& /*event*/)
{
    /* Procitano iz GUI-ja. */
    int symm_rb_ind  = algo_panel.symmetric_rb->GetSelection();
    int asymm_rb_ind = algo_panel.asymmetric_rb->GetSelection();

    int symm_c_ind  = algo_panel.symmetric_c->GetSelection();
    int asymm_c_ind = algo_panel.asymmetric_c->GetSelection();
    int mode_c_ind  = algo_panel.block_mode_c->GetSelection();

    auto in_file_name  = files_panel.input_file_box->GetValue();
    auto out_file_name = files_panel.output_file_box->GetValue();
    /* ** */

    // Zapisi input u objekt tipa nlohman::json.
    json json_in;
    std::ifstream in(in_file_name);
    in >> json_in;

    // Na temelju file-a odredi treba li procitati ili stvoriti omotnicu.
    bool create = json_in["envelope"]["create"];

    SymmetricAlgorithm sa = get_symmetric_algorithm(symm_rb_ind, symm_c_ind);
    AsymmetricAlgorithm aa =
        get_asymmetric_algorithm(asymm_rb_ind, asymm_c_ind);
    SymmetricMode mode = get_symmetric_mode(mode_c_ind);

    if (create) { // Stvori digitalnu omotnicu.
        Envelope::CreateObject c_object(json_in);
        create_envelope(c_object, {out_file_name}, sa, mode, aa, false);
        status_panel.set_status("Digital envelope created successfully!");
    } else { // Procitaj digitalnu omotnicu.
        Envelope::ReadObject r_object(json_in);
        read_envelope(r_object, {out_file_name}, sa, mode, aa, false);
        status_panel.set_status("Digital envelope read successfully!");
    }
}

void ControlsPanel::on_signature_btn_pressed(wxCommandEvent& /*event*/)
{
    /* Procitano iz GUI-ja. */
    int asymm_rb_ind = algo_panel.asymmetric_rb->GetSelection();
    int hash_rb_ind  = algo_panel.hash_rb->GetSelection();

    int asymm_c_ind = algo_panel.asymmetric_c->GetSelection();
    int hash_c_ind  = algo_panel.hash_c->GetSelection();

    auto in_file_name  = files_panel.input_file_box->GetValue();
    auto out_file_name = files_panel.output_file_box->GetValue();
    /* ** */

    // Zapisi input u objekt tipa nlohman::json.
    json json_in;
    std::ifstream in(in_file_name);
    in >> json_in;

    // Na temelju file-a odredi treba li stvoriti ili verificirati potpis.
    bool verify = json_in["signature"]["verify"];

    AsymmetricAlgorithm aa =
        get_asymmetric_algorithm(asymm_rb_ind, asymm_c_ind);
    HashType type        = get_hash(hash_rb_ind, hash_c_ind);
    const auto hash_type = get_hash_type(type);

    if (verify) {
        Signature::VerifyObject v_object(json_in);
        verify_signature(v_object, {out_file_name}, hash_type, aa, false);
        status_panel.set_status("Digital signature verified successfully!");
    } else {
        Signature::CreateObject c_object(json_in);
        create_signature(c_object, {out_file_name}, hash_type, aa, false);
        status_panel.set_status("Digital signature created successfully!");
    }
}

void ControlsPanel::on_seal_btn_pressed(wxCommandEvent& /*event*/)
{
    /* Procitano iz GUI-ja. */
    int symm_rb_ind  = algo_panel.symmetric_rb->GetSelection();
    int asymm_rb_ind = algo_panel.asymmetric_rb->GetSelection();
    int hash_rb_ind  = algo_panel.hash_rb->GetSelection();

    int hash_c_ind  = algo_panel.hash_c->GetSelection();
    int symm_c_ind  = algo_panel.symmetric_c->GetSelection();
    int asymm_c_ind = algo_panel.asymmetric_c->GetSelection();
    int mode_c_ind  = algo_panel.block_mode_c->GetSelection();

    auto in_file_name  = files_panel.input_file_box->GetValue();
    auto out_file_name = files_panel.output_file_box->GetValue();
    /* ** */

    // Zapisi input u objekt tipa nlohman::json.
    json json_in;
    std::ifstream in(in_file_name);
    in >> json_in;

    // Na temelju file-a odredi treba li stvoriti ili procitati pecat.
    bool create = json_in["envelope"]["create"];

    AsymmetricAlgorithm aa =
        get_asymmetric_algorithm(asymm_rb_ind, asymm_c_ind);
    SymmetricAlgorithm sa = get_symmetric_algorithm(symm_rb_ind, symm_c_ind);
    SymmetricMode mode    = get_symmetric_mode(mode_c_ind);

    HashType type        = get_hash(hash_rb_ind, hash_c_ind);
    const auto hash_type = get_hash_type(type);

    // Zbog lakse izrade pecata.
    std::ofstream o(out_file_name, std::ofstream::out | std::ofstream::trunc);

    if (create) { // Stvori pecat.
        Envelope::CreateObject ec_object(json_in);
        Signature::CreateObject sc_object(json_in);

        create_envelope(ec_object, {out_file_name}, sa, mode, aa, true);
        create_signature(sc_object, {out_file_name}, hash_type, aa, true);
        status_panel.set_status("Digital seal created successfully!");
    } else { // Procitaj pecat.
        Envelope::ReadObject er_object(json_in);
        Signature::VerifyObject sv_object(json_in);

        read_envelope(er_object, {out_file_name}, sa, mode, aa, true);
        verify_signature(sv_object, {out_file_name}, hash_type, aa, true);
        status_panel.set_status("Digital seal read successfully!");
    }
}

void ControlsPanel::create_envelope(const Envelope::CreateObject& c_object,
                                    const std::string& file_name,
                                    SymmetricAlgorithm sa,
                                    SymmetricMode mode,
                                    AsymmetricAlgorithm aa,
                                    bool append)
{
    c_object.check_input(sa, aa);
    Botan::AutoSeeded_RNG rng;

    // RSA javni kljuc primatelja i enkriptor.
    Botan::RSA_PublicKey public_key(c_object.n, c_object.e);
    Botan::PK_Encryptor_EME rsa_encryptor(public_key, rng, "EME1(SHA-256)");

    // Stvori digitalnu omotnicu.
    DigitalEnvelope envelope = DigitalEnvelope::create(c_object.symmetric_key,
                                                       c_object.plaintext,
                                                       c_object.iv,
                                                       rsa_encryptor,
                                                       sa,
                                                       mode);

    // U izlaznu datoteku zapisi sadrzaj omotonice.
    Envelope::CreateObject::write_to_file(
        Botan::hex_encode(envelope.get_encrypted_key()),
        Botan::hex_encode(envelope.get_cyphertext()),
        {file_name},
        append);
}

void ControlsPanel::read_envelope(const Envelope::ReadObject& r_object,
                                  const std::string& file_name,
                                  SymmetricAlgorithm sa,
                                  SymmetricMode mode,
                                  AsymmetricAlgorithm aa,
                                  bool append)
{
    // Pogresan input..
    r_object.check_input(sa, aa);

    Botan::AutoSeeded_RNG rng;

    // Stvori omotnicu iz primljenih podataka.
    DigitalEnvelope envelope =
        DigitalEnvelope::create(r_object.encrypted_key, r_object.cyphertext);

    // Privatni kljuc posiljatelja.
    Botan::RSA_PrivateKey key_pair_receiver(
        r_object.p, r_object.q, r_object.e, r_object.d, r_object.n);
    Botan::PK_Decryptor_EME rsa_decryptor(
        key_pair_receiver, rng, "EME1(SHA-256)");

    // Dekriptirani kljuc i tekst u string reprezentaciji.
    const auto decrypted_key =
        Botan::hex_encode(envelope.get_decrypted_key(rsa_decryptor));
    const auto decrypted_text_bytes =
        envelope.get_decrypted_text(decrypted_key, r_object.iv, sa, mode);
    const auto decrypted_text =
        std::string(decrypted_text_bytes.begin(), decrypted_text_bytes.end());

    // U izlaznu datoteku zapisi dektriptirani tekst i kljuc.
    Envelope::ReadObject::write_decr_to_file(
        decrypted_key, decrypted_text, {file_name}, append);
}

void ControlsPanel::create_signature(const Signature::CreateObject& c_object,
                                     const std::string& file_name,
                                     const std::string& hash_type,
                                     AsymmetricAlgorithm aa,
                                     bool append)
{
    c_object.check_input(aa);
    Botan::AutoSeeded_RNG rng;

    Botan::RSA_PrivateKey key_pair_sender(
        c_object.p, c_object.q, c_object.e, c_object.d, c_object.n);
    Botan::PK_Signer signer(
        key_pair_sender, rng, hash_type); // Padding and hash.
    const auto signature = DigitalSignature::create(c_object.plaintext, signer);

    // U izlaznu datoteku zapisi potpis.
    Signature::CreateObject::write_to_file(
        Botan::hex_encode(signature.get_signature()),
        std::string(file_name),
        append);
}

void ControlsPanel::verify_signature(const Signature::VerifyObject& v_object,
                                     const std::string& file_name,
                                     const std::string& hash_type,
                                     AsymmetricAlgorithm aa,
                                     bool append)
{
    v_object.check_input(aa);

    Botan::RSA_PublicKey public_key(v_object.n, v_object.e);
    Botan::PK_Verifier verifier(public_key, hash_type);

    bool correct = DigitalSignature::verify(
        v_object.plaintext, Botan::hex_decode(v_object.signature), verifier);

    v_object.write_to_file(correct, std::string(file_name), append);
}

wxBEGIN_EVENT_TABLE(ControlsPanel, wxPanel) // clang-format off
    EVT_BUTTON(ID::envelope_btn, ControlsPanel::on_envelope_btn_pressed)
    EVT_BUTTON(ID::signature_btn, ControlsPanel::on_signature_btn_pressed)
    EVT_BUTTON(ID::seal_btn, ControlsPanel::on_seal_btn_pressed)
wxEND_EVENT_TABLE() // clang-format on
